package com.javaDevInc.bookNotesControlPanelProject.activemq;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;


public class BookNoteMessageListenerImpl implements MessageListener {
    private static final Logger LOGGER = LogManager.getLogger(BookNoteMessageListenerImpl.class);

    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
                LOGGER.info(((TextMessage) message).getText());
            } catch (JMSException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Wrong message format!");
        }
    }


}
